package springmvc.utils;

/**
 * @author cg
 * @date 2023/6/16 16:32
 */
public class HandlerUtils {
    public static String patternResolve(String path) {
        if (path.length() == 0) return path;
        char first = path.charAt(0);
        return '/' == first ? path : "/" + path;
    }
}
