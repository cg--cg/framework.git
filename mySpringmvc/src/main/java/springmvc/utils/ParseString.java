package springmvc.utils;

/**
 * @author cg
 * @date 2023/6/17 14:21
 */
public interface ParseString {
    /**
     * 将请求路径中的字符串解析为对应的参数
     * @param value 字符串值
     * @return 解析后的对象
     */
    Object parse(String value);
}
