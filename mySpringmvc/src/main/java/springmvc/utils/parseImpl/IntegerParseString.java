package springmvc.utils.parseImpl;

import springmvc.utils.ParseString;

/**
 * @author cg
 * @date 2023/6/17 14:23
 */
public class IntegerParseString implements ParseString {
    @Override
    public Object parse(String value) {
        return Integer.parseInt(value);
    }
}
