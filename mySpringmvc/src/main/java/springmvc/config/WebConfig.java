package springmvc.config;

import java.io.IOException;
import java.util.Properties;

/**
 * @author cg
 * @date 2023/6/17 12:50
 */
public class WebConfig {
    //配置文件
    private Properties config = new Properties();

    //默认视图路径
    private String defaultTemplateDir = "static/";



    public WebConfig(String propertiesPath) {
        try {
            config.load(this.getClass().getClassLoader().getResourceAsStream(propertiesPath));
            init();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    //初始化配置类
    private void init() {

    }

    public String getDefaultTemplateDir() {
        return defaultTemplateDir;
    }

    public void setDefaultTemplateDir(String defaultTemplateDir) {
        this.defaultTemplateDir = defaultTemplateDir;
    }

}
