package springmvc.components.adaptor;

/**
 * @author cg
 * @date 2023/6/17 14:12
 */

/**
 * 封装方法的参数信息
 */
public class MethodParameter {
    private Class<?> clazz;

    private String fieldName;

    private boolean require;

    public MethodParameter(Class<?> clazz, String fieldName, boolean require) {
        this.clazz = clazz;
        this.fieldName = fieldName;
        this.require = require;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public String getFieldName() {
        return fieldName;
    }

    public boolean isRequire() {
        return require;
    }
}
