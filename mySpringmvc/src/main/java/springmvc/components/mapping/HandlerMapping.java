package springmvc.components.mapping;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cg
 * @date 2023/6/16 16:00
 */

/**
 * 封装处理器的信息
 */
public class HandlerMapping {
    //处理器的请求映射规则
    private Pattern pattern;
    //处理器方法
    private Method method;
    //处理器所处的类
    private Object controller;
    //请求方法类型
    private String type = "";
    //是否需要渲染。根据RequestBody判断
    private boolean isRender = true;

    public HandlerMapping() {
    }

    public HandlerMapping(Pattern pattern, Method method, Object controller) {
        this.pattern = pattern;
        this.method = method;
        this.controller = controller;
    }

    public boolean support(String url, String method) {
        //匹配url
        Matcher matcher = pattern.matcher(url);
        return matcher.matches() && ("".equals(type) || type.equals(method));
    }

    public boolean isRender() {
        return isRender;
    }

    public void setRender(boolean render) {
        isRender = render;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }
}
