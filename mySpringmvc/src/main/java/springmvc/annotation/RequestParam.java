package springmvc.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/17 13:59
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface RequestParam {
    String value();
    //是否必须需要
    boolean require() default true;
}
