package springmvc.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/16 16:16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
public @interface RequestMapping {
    String value() default "";
}
