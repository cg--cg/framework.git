package springmvc.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/16 16:21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface GetMapping {
    String value() default "";
}
