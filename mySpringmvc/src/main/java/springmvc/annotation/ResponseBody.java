package springmvc.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/16 17:31
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
public @interface ResponseBody {
}
