package test.controller;

import spring.annotation.bean.Controller;
import springmvc.annotation.PostMapping;
import springmvc.components.view.ModelAndView;
import springmvc.annotation.RequestMapping;
import test.pojo.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cg
 * @date 2023/6/16 16:29
 */
@Controller
@RequestMapping("/web")
public class UserController {
    @PostMapping("login")
    public ModelAndView login(User user, String path) {
        System.out.println("path = " + path);

        String username = user.getUsername();
        String password = user.getPassword();
        //模拟each渲染
        List<User> userList = new ArrayList<>();
        userList.add(new User("张三", "111"));
        userList.add(new User("李四", "222"));
        userList.add(new User("王五", "333"));
        ModelAndView mv = new ModelAndView();
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("password", password);
        map.put("userList", userList);
        mv.setModel(map);
        mv.setViewName("index");
        return mv;
    }
}
