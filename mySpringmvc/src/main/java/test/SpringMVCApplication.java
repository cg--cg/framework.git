package test;

import server.BootStrap;
import spring.annotation.ComponentScan;

/**
 * @author cg
 * @date 2023/6/16 14:47
 */
@ComponentScan
public class SpringMVCApplication {
    public static void main(String[] args) {
        BootStrap bootStrap = new BootStrap(SpringMVCApplication.class);

        bootStrap.start();
    }
}
