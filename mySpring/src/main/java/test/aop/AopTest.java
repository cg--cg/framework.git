package test.aop;

import spring.aop.ProceedingJoinPoint;
import spring.aop.annotation.After;
import spring.aop.annotation.Aspect;
import spring.aop.annotation.Before;
import spring.aop.annotation.PointCut;
import spring.annotation.bean.Component;

/**
 * @author cg
 * @date 2023/6/14 17:08
 */
@Component
@Aspect
public class AopTest {
    @PointCut("spring.test.CglibTest")
    public void pointcut(){}

    @Before
    public void before(ProceedingJoinPoint joinPoint) {
        String beanName = joinPoint.getBeanName();
        System.out.println(beanName + "aop方法执行前");
    }

    @After
    public void after(ProceedingJoinPoint joinPoint){
        String beanName = joinPoint.getBeanName();
        System.out.println(beanName + "aop方法执行后");
    }
}
