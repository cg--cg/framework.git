package test.aop;

import spring.annotation.bean.Component;
import spring.aop.ProceedingJoinPoint;
import spring.aop.annotation.After;
import spring.aop.annotation.Aspect;
import spring.aop.annotation.Before;
import spring.aop.annotation.PointCut;

/**
 * @author cg
 * @date 2023/6/14 17:08
 */
@Component
@Aspect
public class AopTest2 {
    @PointCut("spring.test.CglibTest")
    public void pointcut(){}

    @Before
    public void before(ProceedingJoinPoint joinPoint) {
        String beanName = joinPoint.getBeanName();
        System.out.println(beanName + "aop方法执行前2");
    }

    @After
    public void after(ProceedingJoinPoint joinPoint){
        String beanName = joinPoint.getBeanName();
        System.out.println(beanName + "aop方法执行后2");
    }
}
