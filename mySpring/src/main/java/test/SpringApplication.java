package test;

import spring.ApplicationContext;
import spring.annotation.ComponentScan;

/**
 * @author cg
 * @date 2023/6/14 14:45
 */
@ComponentScan
public class SpringApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ApplicationContext(SpringApplication.class);
        applicationContext.refresh();
//        String[] beanNames = applicationContext.getBeanNames();
//        for (String beanName : beanNames) {
//            System.out.println("beanName = " + beanName);
//            Object bean1 = applicationContext.getBean(beanName);
//            Object bean2 = applicationContext.getBean(beanName);
//            System.out.println("bean1 = " + bean1);
//            System.out.println("bean2 = " + bean2);
//            System.out.println("-----------------------------");
//        }

        System.out.println("循环依赖测试开始*******************************");
//        CglibTest cglibTest = (CglibTest) applicationContext.getBean("cglibTest");
//        User user = applicationContext.getBean(User.class);
//        user.print();
//        UserService userService = applicationContext.getBean(UserService.class);
//        userService.print();
        CglibTest cglibTest = applicationContext.getBean(CglibTest.class);
        cglibTest.print();
        System.out.println("循环依赖测试结束*******************************");

    }
}
