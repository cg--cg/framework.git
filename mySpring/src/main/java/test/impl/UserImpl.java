package test.impl;

import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Component;
import spring.annotation.bean.Scope;
import test.CglibTest;
import test.User;

/**
 * @author cg
 * @date 2023/6/14 15:44
 */
@Component
@Scope("prototype")//测试循环依赖的时候需要取消注释
public class UserImpl implements User {
    @AutoWired
    private CglibTest cglibTest;

    public void print(){
        System.out.println("User中注入了：" + cglibTest);
//        cglibTest.print();
    }
}
