package test.impl;

import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Service;
import test.User;
import test.UserService;

/**
 * @author cg
 * @date 2023/6/14 15:43
 */
@Service
public class UserServiceImpl implements UserService {
    @AutoWired
    private User user;

    public void print(){
        System.out.println("userService中注入了：" + user);
        user.print();
    }
}
