package test;

import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Component;
import test.impl.UserServiceImpl;

/**
 * @author cg
 * @date 2023/6/15 14:19
 */
@Component
public class CglibTest {
    @AutoWired
    private UserServiceImpl userService;

    public void print() {
        System.out.println("cglibTest中注入了：" + userService);
        userService.print();
    }
}
