package spring.aop;

import spring.utils.BeanUtils;

import java.lang.reflect.Method;

/**
 * @author cg
 * @date 2023/6/15 15:45
 */

/**
 * 切面拦截点，保存拦截处的所有信息
 */
public class ProceedingJoinPoint {
    private String beanName;

    private Class<?> clazz;

    private Method method;

    private Object[] args;
    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public void load(Object target,Method method,Object[] args) {
        this.beanName = BeanUtils.getDefaultBeanName(target.getClass());
        this.clazz = target.getClass();
        this.method = method;
        this.args = args;
    }
}
