package spring.aop.utils;

/**
 * @author cg
 * @date 2023/6/22 12:58
 */
public class PublicAopMatch implements AopMatch{
    @Override
    public boolean match(String pointcut, String pattern) {
        //模仿url路径匹配规则
        int len = pattern.length();
        int length = pointcut.length();
        for (int i = 0; i < length && i < len; i++) {
            char ch1 = pattern.charAt(i);
            char ch2 = pointcut.charAt(i);
            if ('*' == ch2 && pattern.charAt(i - 1) == '.') {
                return true;
            }
            if (ch1 != ch2) {
                return false;
            }
        }
        return len == length;
    }
}
