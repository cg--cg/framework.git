package spring.aop.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cg
 * @date 2023/6/22 13:06
 */
public class AopMatchContext {
    private static final Map<String, AopMatch> aopMatchMap = new HashMap<>();
    static {
        aopMatchMap.put("public", new PublicAopMatch());
        aopMatchMap.put("annotation", new AnnotationAopMatch());
        aopMatchMap.put("", new PublicAopMatch());
    }

    public static AopMatch getAopMatch(String type) {
        return aopMatchMap.get(type);
    }
}
