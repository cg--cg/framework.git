package spring.aop.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author cg
 * @date 2023/6/22 13:00
 */
public class AnnotationAopMatch implements AopMatch{
    @Override
    public boolean match(String pointcut, String pattern) {
        try {
            Class<?> clazz = Class.forName(pattern);
            Class<Annotation> annotation = (Class<Annotation>) Class.forName(pointcut);
            for (Method declaredMethod : clazz.getDeclaredMethods()) {
                declaredMethod.setAccessible(true);
                if (declaredMethod.isAnnotationPresent(annotation)) {
                    return true;
                }
            }
            return false;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
