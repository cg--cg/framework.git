package spring.aop.utils;

/**
 * @author cg
 * @date 2023/6/22 12:57
 */
public interface AopMatch {

    /**
     * 匹配切面和类关系
     * @param pointcut
     * @param pattern
     * @return
     */
    boolean match(String pointcut, String pattern);
}
