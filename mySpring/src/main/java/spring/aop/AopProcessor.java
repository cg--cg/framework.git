package spring.aop;

import spring.aop.utils.AopMatch;

import java.lang.reflect.Method;

/**
 * @author cg
 * @date 2023/6/14 16:44
 */
public class AopProcessor {
    private Class<?> clazz;//被代理对象

    private String pointCut;//切点
    private Method beforeMethod;//环绕前方法
    private Method afterMethod;//环绕后方法

    private Method aroundMethod;

    private AopMatch aopMatch;

    public AopMatch getAopMatch() {
        return aopMatch;
    }

    public void setAopMatch(AopMatch aopMatch) {
        this.aopMatch = aopMatch;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getPointCut() {
        return pointCut;
    }

    public void setPointCut(String pointCut) {
        this.pointCut = pointCut;
    }

    public Method getBeforeMethod() {
        return beforeMethod;
    }

    public void setBeforeMethod(Method beforeMethod) {
        this.beforeMethod = beforeMethod;
    }

    public Method getAfterMethod() {
        return afterMethod;
    }

    public void setAfterMethod(Method afterMethod) {
        this.afterMethod = afterMethod;
    }

    public Method getAroundMethod() {
        return aroundMethod;
    }

    public void setAroundMethod(Method aroundMethod) {
        this.aroundMethod = aroundMethod;
    }
}
