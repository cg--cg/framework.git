package spring.aop.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/14 16:58
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Aspect {
}
