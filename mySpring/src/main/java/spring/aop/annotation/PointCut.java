package spring.aop.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/4/25 10:15
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PointCut {

    String value() default "";
}
