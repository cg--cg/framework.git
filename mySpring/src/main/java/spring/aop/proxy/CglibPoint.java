package spring.aop.proxy;

import java.lang.reflect.Method;

/**
 * @author cg
 * @date 2023/6/18 21:42
 */
public interface CglibPoint {
    //处理代理对象的代理功能，是责任链的调用链方法
    Object proceed(CglibChain chain, Method method, Object[] args);
}
