package spring.annotation.bean;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/4/24 14:15
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Component {
    String value() default "";
}
