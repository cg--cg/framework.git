package spring.annotation.bean;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/14 17:30
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutoWired {
    boolean require() default true;
}
