package spring.annotation.bean;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/14 16:30
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Lazy {
}
