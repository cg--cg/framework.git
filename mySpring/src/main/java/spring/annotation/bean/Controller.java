package spring.annotation.bean;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/4/25 10:15
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Controller {
    String value() default "";
}
