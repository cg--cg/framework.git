package spring.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/4/24 14:20
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
/**
 * 定义包扫描路径
 */
public @interface ComponentScan {

    String value() default "";
}
