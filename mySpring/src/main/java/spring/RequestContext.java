package spring;

/**
 * @author cg
 * @date 2023/6/19 9:11
 */


/**
 * 存放http请求内容
 */
public class RequestContext {
    private static final ThreadLocal<Object> httpThreadLocal = new ThreadLocal<>();

    public static Object getHttpRequest() {
        return httpThreadLocal.get();
    }

    public static void setHttpRequest(Object httpRequest) {
        httpThreadLocal.set(httpRequest);
    }

    public static void removeHttpRequest() {
        httpThreadLocal.remove();
    }
}
