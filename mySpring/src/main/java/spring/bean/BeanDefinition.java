package spring.bean;

/**
 * @author cg
 * @date 2023/6/14 15:00
 */

/**
 * 封装bean对象
 */
public class BeanDefinition {
    /**
     * bean的类文件
     */
    private Class<?> clazz;

    /**
     * bean的名字，唯一标识
     */
    private String beanName;

    /**
     * 是否懒加载
     */
    private boolean isLazy = false;
    /**
     * 是否是单例bean
     */
    private boolean isSingleton = true;

    public BeanDefinition(Class<?> clazz, String beanName) {
        this.clazz = clazz;
        this.beanName = beanName;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public boolean isLazy() {
        return isLazy;
    }

    public void setLazy(boolean lazy) {
        isLazy = lazy;
    }

    public boolean isSingleton() {
        return isSingleton;
    }

    public void setSingleton(boolean singleton) {
        isSingleton = singleton;
    }

}
