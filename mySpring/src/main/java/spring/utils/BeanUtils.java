package spring.utils;

/**
 * @author cg
 * @date 2023/6/14 17:36
 */
public class BeanUtils {
    public static String getDefaultBeanName(Class<?> clazz) {
        String beanName;
//        for (Class<?> clazzInterface : clazz.getInterfaces()) {
//            //如果实现了接口，则默认为接口名小写
//            clazz = clazzInterface;
//            break;
//        }
        String simpleName = clazz.getSimpleName();
        char first = simpleName.charAt(0);
        beanName = Character.toLowerCase(first) + simpleName.substring(1);
        return beanName;
    }

    public static boolean aopMatch(String pointcut, String pattern) {
        //模仿url路径匹配规则
        int len = pattern.length();
        int length = pointcut.length();
        for (int i = 0; i < length && i < len; i++) {
            char ch1 = pattern.charAt(i);
            char ch2 = pointcut.charAt(i);
            if ('*' == ch2 && pattern.charAt(i - 1) == '.') {
                return true;
            }
            if (ch1 != ch2) {
                return false;
            }
        }
        return len == length;
    }
}
