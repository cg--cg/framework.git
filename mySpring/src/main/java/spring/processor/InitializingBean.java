package spring.processor;

/**
 * @author cg
 * @date 2023/6/14 18:03
 */
public interface InitializingBean {
    /**
     * 自定义bean初始化逻辑
     * @throws Exception
     */
    void afterPropertiesSet() throws Exception;
}
