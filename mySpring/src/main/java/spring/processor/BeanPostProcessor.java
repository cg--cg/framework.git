package spring.processor;

/**
 * @author cg
 * @date 2023/6/14 16:44
 */

/**
 * 自定义实现类即可在bean初始化前后执行操作
 */
public interface BeanPostProcessor {
    default Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    default Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }
}
