手写spring框架，实现ioc、aop以及依赖注入功能，并且使用三级缓存的思想解决循环依赖问题。<br>

博客地址：<br>
* <a href = "https://blog.csdn.net/qq_44993268/article/details/131230877?spm=1001.2014.3001.5501"> 手写spring框架预备知识：三级缓存解决循环依赖问题</a><br>
* <a href = "https://blog.csdn.net/qq_44993268/article/details/131282078?spm=1001.2014.3001.5501"> 手写spring框架预备知识：责任链模式解决cglib多重代理问题</a><br>
* <a href = "https://blog.csdn.net/qq_44993268/article/details/131232416?spm=1001.2014.3001.5501"> 手写spring框架：实现ioc和aop功能；使用三级缓存解决循环依赖问题</a><br>
测试截图:<br>
ioc容器和循环依赖<br>
![img.png](img.png)<br>
aop和扩展功能<br>
![img_1.png](img_1.png)<br>