package com.cg.demo.service;


import com.cg.demo.pojo.User;
import server.response.HttpResponse;
import springmvc.components.view.ModelAndView;

/**
 * @author cg
 * @date 2023/6/15 14:07
 */
public interface UserService {

    ModelAndView login(User user, HttpResponse response);

    ModelAndView regist(User user);

}
