package com.cg.demo.service.impl;


import com.cg.demo.mapper.UserMapper;
import com.cg.demo.pojo.User;
import com.cg.demo.service.UserService;
import server.response.HttpResponse;
import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Service;
import springmvc.components.view.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cg
 * @date 2023/6/14 15:43
 */
@Service
public class UserServiceImpl implements UserService {
    @AutoWired
    private UserMapper userMapper;

    public ModelAndView login(User user, HttpResponse response) {
        String username = user.getUsername();
        User userByName = userMapper.getUserByName(username);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("login_success");
        Map<String, Object> model = new HashMap<>();
        if (userByName != null && userByName.getPassword().equals(user.getPassword())) {
            model.put("username", username);
        }else {
            return null;
        }
        response.setCookie("username", username);
        mv.setModel(model);
        return mv;
    }

    @Override
    public ModelAndView regist(User user) {
        Integer integer = userMapper.insertUser(user);
        if (integer > 0) {
            return new ModelAndView("regist_success");
        } else return null;
    }


}
