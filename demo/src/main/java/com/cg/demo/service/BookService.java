package com.cg.demo.service;

import com.cg.demo.pojo.Book;

import java.util.List;

/**
 * @author cg
 * @date 2023/6/21 18:23
 */
public interface BookService {
    List<Book> getAllBooksPage(Integer cur, Integer size);

    List<Book> getAll();

    Integer addBook(Book book);

    Integer deleteBook(Integer id);

    Integer updateBook(Book book);

    Book getBook(Integer id);
}
