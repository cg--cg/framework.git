package com.cg.demo.service.impl;

import com.cg.demo.mapper.BookMapper;
import com.cg.demo.pojo.Book;
import com.cg.demo.service.BookService;
import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Service;

import java.util.List;

/**
 * @author cg
 * @date 2023/6/21 18:25
 */
@Service
public class BookServiceImpl implements BookService {

    @AutoWired
    private BookMapper bookMapper;

    @Override
    public List<Book> getAllBooksPage(Integer cur, Integer size) {

        return bookMapper.getAllPage(cur * 4, size);
    }

    @Override
    public List<Book> getAll() {
        return bookMapper.getAll();
    }

    @Override
    public Integer addBook(Book book) {
        return bookMapper.addBook(book);
    }

    @Override
    public Integer deleteBook(Integer id) {
        return bookMapper.deleteBook(id);
    }

    @Override
    public Integer updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public Book getBook(Integer id) {
        return bookMapper.getBookById(id);
    }
}
