package com.cg.demo.mapper;

import com.cg.demo.pojo.Book;
import mybatis.annotation.Param;

import java.util.List;

/**
 * @author cg
 * @date 2023/6/21 18:23
 */
public interface BookMapper {


    List<Book> getAllPage(@Param("cur") Integer cur, @Param("size") Integer size);

    List<Book> getAll();

    Integer addBook(@Param("book") Book book);

    Integer deleteBook(@Param("id") Integer id);

    Integer updateBook(@Param("book") Book book);

    Book getBookById(@Param("id") Integer id);
}
