/**
 * 
 */
package com.cg.demo.mapper;

import com.cg.demo.pojo.User;
import mybatis.annotation.Param;

import java.util.List;

/**
 * @author cg
 * @date 2023/6/21 10:30
 */
public interface UserMapper
{

    /**
     * 获取单个user
     */
    User getUserByName(@Param("username") String userName);
    
    /**
     * 获取所有用户
     */
    List<User> getAll();

    Integer insertUser(User user);
}
