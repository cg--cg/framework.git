package com.cg.demo.controller;

import com.cg.demo.annotation.HasRole;
import com.cg.demo.pojo.Book;
import com.cg.demo.service.BookService;
import server.request.HttpRequest;
import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Controller;
import springmvc.annotation.GetMapping;
import springmvc.annotation.PostMapping;
import springmvc.components.view.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cg
 * @date 2023/6/21 16:44
 */
@Controller
public class IndexController {
    @AutoWired
    private BookService bookService;

    /**
     * 首页
     *
     * @return
     */
    @GetMapping("/")
    public ModelAndView index(HttpRequest request) {
        List<Book> books = bookService.getAllBooksPage(0, 4);
        ModelAndView mv = new ModelAndView("index");
        Map<String, Object> model = new HashMap<>();
        model.put("books", books);
        model.put("cur", 0);
        model.put("username", request.getCookie("username"));
        List<Book> bookList = bookService.getAll();
        int total = bookList.size();
        total = (total + 3) / 4;
        model.put("total", total);
        List<Integer> pages = new ArrayList<>();
        for (int i = 0; i < total; i++) {
            pages.add(i + 1);
        }
        model.put("pages", pages);
        mv.setModel(model);
        return mv;
    }

    /**
     * 带着请求数据返回编辑页面
     * @param id
     * @return
     */
    @GetMapping("/editBook")
    public ModelAndView editBook(Integer id) {
        if (id == -1) {
            return new ModelAndView("book_edit");
        }
        Book book = bookService.getBook(id);
        ModelAndView mv = new ModelAndView("book_edit");
        Map<String, Object> model = new HashMap<>();
        model.put("book", book);
        mv.setModel(model);

        return mv;
    }

    /**
     * 图书管理页
     *
     * @return
     */
    @GetMapping("/manager")
    public ModelAndView manager() {
        return getModelAndView();
    }

    /**
     * 添加和修改图书
     *
     * @param book
     * @return
     */
    @HasRole("admin")
    @PostMapping("/addBook")
    public ModelAndView addBook(Book book) {
        if (book.getId() != null) {
            //不是空id就判断为修改
            bookService.updateBook(book);
        } else {
            bookService.addBook(book);
        }
        return getModelAndView();
    }

    /**
     * 删除图书，需要校验管理员权限
     *
     * @param id
     * @return
     */
    @HasRole("admin")
    @GetMapping("/deleteBook")
    public ModelAndView deleteBook(Integer id) {
        bookService.deleteBook(id);
        return getModelAndView();
    }

    @GetMapping("/page")
    public ModelAndView index(HttpRequest request, Integer cur, Integer size) {
        List<Book> books = bookService.getAllBooksPage(cur - 1, size);
        ModelAndView mv = new ModelAndView("index");
        Map<String, Object> model = new HashMap<>();
        model.put("books", books);
        model.put("cur", cur);
        model.put("username", request.getCookie("username"));
        List<Book> bookList = bookService.getAll();
        int total = bookList.size();
        total = (total + size - 1) / size;
        model.put("total", total);
        List<Integer> pages = new ArrayList<>();
        for (int i = 0; i < total; i++) {
            pages.add(i + 1);
        }
        model.put("pages", pages);
        mv.setModel(model);
        return mv;
    }

    private ModelAndView getModelAndView() {
        List<Book> books = bookService.getAll();
        ModelAndView mv = new ModelAndView("book_manager");
        Map<String, Object> model = new HashMap<>();
        model.put("books", books);
        mv.setModel(model);
        return mv;
    }
}
