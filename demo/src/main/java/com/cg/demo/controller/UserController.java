package com.cg.demo.controller;


import com.cg.demo.pojo.User;
import com.cg.demo.service.UserService;
import server.request.HttpRequest;
import server.response.HttpResponse;
import spring.annotation.bean.AutoWired;
import spring.annotation.bean.Controller;
import springmvc.annotation.GetMapping;
import springmvc.annotation.PostMapping;
import springmvc.components.view.ModelAndView;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author cg
 * @date 2023/6/16 16:29
 */
@Controller
public class UserController {
    @AutoWired
    private UserService userService;


    @PostMapping("login")
    public ModelAndView login(User user, HttpResponse response) {
        return userService.login(user,response);
    }

    @PostMapping("regist")
    public ModelAndView regist(User user) {
        return userService.regist(user);
    }

    @GetMapping("logout")
    public String logout(HttpResponse response) {
        response.setCookie("username", "; Expires=Thu, 01 Jan 1970 00:00:00 GMT");
        return "logout_success";
    }

}
