package com.cg.demo.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/22 13:12
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface HasRole {
    String value();
}
