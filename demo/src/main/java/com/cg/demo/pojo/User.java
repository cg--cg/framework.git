package com.cg.demo.pojo;

import lombok.Data;

/**
 * @author cg
 * @date 2023/6/21 10:30
 */
@Data
public class User {
    private Long id;

    private String username;

    private String password;

    private String email;

}
