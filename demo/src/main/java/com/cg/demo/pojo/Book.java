package com.cg.demo.pojo;

import lombok.Data;

/**
 * @author cg
 * @date 2023/6/21 18:22
 */
@Data
public class Book {
    private Long id;
    private String name;
    private int price;
    private String author;
    private int sales;//销量
    private int stock;//库存
    private String imgPath = "static/img/default.jpg";//封面
}
