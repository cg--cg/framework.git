package com.cg.demo;

import spring.annotation.ComponentScan;
import starter.SpringApplication;
import starter.MapperScan;

/**
 * @author cg
 * @date 2023/6/21 11:16
 */
@ComponentScan
@MapperScan("com.cg.demo.mapper")
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class);
    }
}
