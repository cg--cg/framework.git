package com.cg.demo.aop;

import spring.annotation.bean.Component;
import spring.aop.ProceedingJoinPoint;
import spring.aop.annotation.After;
import spring.aop.annotation.Aspect;
import spring.aop.annotation.Before;
import spring.aop.annotation.PointCut;

import java.lang.reflect.Method;

/**
 * @author cg
 * @date 2023/6/22 12:45
 */
@Component
@Aspect
public class LogAspect {
    @PointCut("public:com.cg.demo.controller.*")
    public void pointcut() {

    }

    @Before
    public void before(ProceedingJoinPoint joinPoint) {
        String controller = joinPoint.getBeanName();
        Method method = joinPoint.getMethod();
        String methodName = method.getName();
        System.out.println(controller + "的" + methodName + "方法收到请求");
    }

    @After
    public void after(ProceedingJoinPoint joinPoint) {
        String controller = joinPoint.getBeanName();
        Method method = joinPoint.getMethod();
        String methodName = method.getName();
        System.out.println(controller + "的" + methodName + "方法结束请求");

    }
}
