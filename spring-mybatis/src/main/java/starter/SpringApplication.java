package starter;

import server.BootStrap;

/**
 * @author cg
 * @date 2023/6/23 12:26
 */
public class SpringApplication {

    public static void run(Class<?> config) {
        BootStrap bootStrap = new BootStrap(config);

        bootStrap.start();
    }
}
