package starter;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/21 19:29
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface MapperScan {
    String value();
}
