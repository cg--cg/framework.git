package server.servlet;

import server.request.HttpRequest;
import server.response.HttpResponse;

/**
 * @author cg
 * @date 2023/6/13 9:35
 */
public class Login extends HttpServlet{

    @Override
    protected void doGet(HttpRequest httpRequest, HttpResponse httpResponse) {

    }

    @Override
    protected void doPost(HttpRequest httpRequest, HttpResponse httpResponse) {
        String username = httpRequest.getBodyParameter("username");
        String password = httpRequest.getBodyParameter("password");
        if ("admin".equals(username) && "admin".equals(password)) {
            httpResponse.writeAndFlush("登录成功");
        }else {
            httpResponse.writeAndFlush("登录失败");
        }
    }
}
