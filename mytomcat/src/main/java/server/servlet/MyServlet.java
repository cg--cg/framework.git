package server.servlet;

import server.request.HttpRequest;
import server.response.HttpResponse;
/**
 * @author cg
 * @date 2023/6/7 15:48
 */
public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpRequest httpRequest, HttpResponse httpResponse) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        httpResponse.writeAndFlush("MyServlet--get请求" + httpRequest.getUrl());
    }


    @Override
    protected void doPost(HttpRequest httpRequest, HttpResponse httpResponse) {

        httpResponse.writeAndFlush("MyServlet--post请求");

    }
}
