package server.servlet;

import server.request.HttpRequest;
import server.response.HttpResponse;

/**
 * @author cg
 * @date 2023/6/7 15:44
 */
public interface Servlet {
    /**
     * 传入一个初始化配置类
     */
    void init(Class<?> config) throws Exception;

    void destroy() throws Exception;

    void service(HttpRequest httpRequest, HttpResponse httpResponse) throws Exception;
}
