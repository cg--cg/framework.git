package server.request;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author cg
 * @date 2023/6/7 13:23
 */
public class HttpRequest {
    /**
     * 请求方式
     */
    private String method;

    /**
     * 请求路径
     */
    private String url;
    /**
     * 请求参数列表
     */
    private Map<String, String> parameterMap = new HashMap<>();

    /**
     * 请求体参数列表
     */
    private Map<String, String> requestBodyMap;

    private InputStream inputStream;

    private Map<String, String> headers;

    private Map<String, String> cookies;

    public String getCookie(String key) {
        return this.cookies.get(key);
    }
    public Map<String, String> getCookies() {
        return cookies;
    }

    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getHeader(String headerName) {
        return headers.get(headerName);
    }
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getPathParameter(String parameterName) {
        return parameterMap.get(parameterName);
    }

    public String getBodyParameter(String parameterName) {
        return requestBodyMap.get(parameterName);
    }

    public Map<String, String> getRequestBodyMap() {
        return requestBodyMap;
    }

    public void setRequestBodyMap(Map<String, String> requestBodyMap) {
        this.requestBodyMap = requestBodyMap;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, String> parameterMap) {
        this.parameterMap = parameterMap;
    }
}
