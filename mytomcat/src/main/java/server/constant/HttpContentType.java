package server.constant;

/**
 * @author cg
 * @date 2023/6/7 15:24
 * http请求头中的content-type
 */
public class HttpContentType {
    public static String JAVASCRIPT = "application/javascript";

    public static String TEXT = "text/html";

    public static String TEXT_CSS = "text/css";

    public static String IMAGE_JPEG = "image/jpeg";

    public static String IMAGE_PNG = "image/png";

    public static String JIF = "image/png";
}
