package server.constant;

/**
 * @author cg
 * @date 2023/6/7 15:00
 * http的状态码
 */
public class HttpStatusCode {
    public static final String SUCCESS = "HTTP/1.1 200 OK \n";

    public static final String NOTFOUND = "HTTP/1.1 404 NOT Found \n";
}
