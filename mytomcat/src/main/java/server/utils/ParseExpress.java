package server.utils;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * @author cg
 * @date 2023/6/12 15:16
 */
public class ParseExpress {
    private static final ScriptEngineManager manager = new ScriptEngineManager();

    public static Long parseLong(String expression) {
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        Object result = null;
        try {
            result = engine.eval(expression);
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
        return ((Number) result).longValue();
    }

}
