package server.servlet;

import server.request.HttpRequest;
import server.response.HttpResponse;

/**
 * @author cg
 * @date 2023/6/7 15:45
 */
public abstract class HttpServlet implements Servlet {

    protected abstract void doGet(HttpRequest httpRequest, HttpResponse httpResponse);

    protected abstract void doPost(HttpRequest httpRequest, HttpResponse httpResponse);

    @Override
    public void init(Class<?> config) throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void service(HttpRequest httpRequest, HttpResponse httpResponse) {
        //请求分发
        if ("GET".equals(httpRequest.getMethod())) doGet(httpRequest, httpResponse);
        else doPost(httpRequest, httpResponse);
    }
}
