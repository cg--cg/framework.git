package server.config;

import java.io.IOException;
import java.util.Properties;

/**
 * @author cg
 * @date 2023/6/27 10:58
 */
public class NettyConfig {
    private Properties properties;

    public NettyConfig(String parameterConfig) {
        this.properties = new Properties();
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream(parameterConfig));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
