package server.netty;

import io.netty.channel.Channel;
import server.request.HttpRequest;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cg
 * @date 2023/6/27 9:40
 */
public class ChannelRequestHandler {
    //保存channel和request的对应关系
    private static final Map<Channel, HttpRequest> channelRequest = new ConcurrentHashMap<>();

    public static HttpRequest getRequest(Channel channel) {
        return channelRequest.get(channel);
    }

    public static void setRequest(Channel channel, HttpRequest request) {
        channelRequest.put(channel, request);
    }

    public static HttpRequest removeChannel(Channel channel) {
        return channelRequest.remove(channel);
    }
}
