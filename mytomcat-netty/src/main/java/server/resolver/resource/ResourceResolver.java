package server.resolver.resource;

/**
 * @author cg
 * @date 2023/6/7 14:09
 */

import server.response.HttpResponse;

import java.io.File;
import java.io.IOException;

/**
 * 静态资源解析器
 */
public interface ResourceResolver {
    /**
     * 单次接收数据的缓冲区大小
     */
    int bufferSize = 1024;

    /**
     * 解析静态文件
     * @param file 文件名
     * @param httpResponse 请求响应
     * @param contentType 响应类型
     * @throws IOException
     */
    void resolve(File file, HttpResponse httpResponse, String contentType) throws IOException;
}
