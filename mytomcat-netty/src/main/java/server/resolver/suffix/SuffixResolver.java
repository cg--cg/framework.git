package server.resolver.suffix;

import server.constant.HttpContentType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cg
 * @date 2023/6/7 15:31
 *
 * 解析文件后缀为请求头中的content-type
 */
public class SuffixResolver {
    private static final Map<String, String> suffixResolverMap;
    static {
        suffixResolverMap = new HashMap<>();
        suffixResolverMap.put(".html", HttpContentType.TEXT);
        suffixResolverMap.put(".htm", HttpContentType.TEXT);
        suffixResolverMap.put(".jpg", HttpContentType.IMAGE_JPEG);
        suffixResolverMap.put(".jpeg", HttpContentType.IMAGE_JPEG);
        suffixResolverMap.put(".png", HttpContentType.IMAGE_PNG);
        suffixResolverMap.put(".gif", HttpContentType.IMAGE_JPEG);
        suffixResolverMap.put(".css", HttpContentType.TEXT_CSS);
        suffixResolverMap.put(".js", HttpContentType.JAVASCRIPT);
    }

    public static String suffixResolver(String suffix) {
        return suffixResolverMap.getOrDefault(suffix,HttpContentType.TEXT);
    }

    public static void registSuffixResolver(String suffix, String contentType) {
        suffixResolverMap.put(suffix, contentType);
    }
}
