package mybatis.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cg
 * @date 2023/6/20 17:17
 */
public class StringUtils {
    /**
     * 驼峰转下划线
     * @param input
     * @return
     */
    public static String camelToUnderscore(String input) {
        if (input == null) return null;
        String regex = "([a-z])([A-Z]+)";
        String replacement = "$1_$2";
        return input.replaceAll(regex, replacement).toLowerCase();
    }

    /**
     * 下划线转驼峰
     * @param input
     * @return
     */
    public static String underscoreToCamel(String input) {
        if (input == null) return null;
        StringBuilder sb = new StringBuilder(input);
        Matcher m = Pattern.compile("_([a-z])").matcher(input);
        int i = 0;
        while (m.find()) {
            sb.replace(m.start() - i, m.end() - i, m.group(1).toUpperCase());
            i++;
        }
        return sb.toString();
    }
}
