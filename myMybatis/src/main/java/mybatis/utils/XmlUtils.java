package mybatis.utils;

import mybatis.config.Configuration;
import mybatis.mapper.MapperStatement;
import mybatis.mapper.SqlType;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cg
 * @date 2023/6/20 14:55
 */
public class XmlUtils {
    /**
     * 解析xml并封装到sqlConfig中
     *
     * @param xmlName
     * @param configuration
     */
    public static void readMapperXml(File xmlName, Configuration configuration) {
        try {

            // 创建一个读取器
            SAXReader saxReader = new SAXReader();
            saxReader.setEncoding("utf-8");

            // 读取文件内容
            Document document = saxReader.read(xmlName);

            // 获取xml中的根元素
            Element rootElement = document.getRootElement();

            // 根元素必须是mapper标签
            if (!"mapper".equals(rootElement.getName())) {
                System.err.println("mapper xml文件根元素不是mapper");
                return;
            }
            //解析响应的标签
            String namespace = rootElement.attributeValue("namespace");
            //解析namespace下的所有方法标签
            for (Iterator iterator = rootElement.elementIterator(); iterator.hasNext(); ) {
                Element element = (Element) iterator.next();
                String eleName = element.getName();
                //解析为MapperStatement类
                MapperStatement statement = new MapperStatement();

                if (SqlType.SELECT.value().equals(eleName)) {
                    String resultType = element.attributeValue("resultType");
                    statement.setResultType(resultType);
                    statement.setSqlCommandType(SqlType.SELECT);
                } else if (SqlType.UPDATE.value().equals(eleName)) {
                    statement.setSqlCommandType(SqlType.UPDATE);
                } else {
                    // 其他标签自己实现
                    System.err.println("不支持此xml标签解析:" + eleName);
                    statement.setSqlCommandType(SqlType.DEFAULT);
                }
                //设置SQL的唯一ID
                String sqlId = namespace + "." + element.attributeValue("id");

                statement.setSqlId(sqlId);
                statement.setNamespace(namespace);
                String sql = element.getStringValue().trim();
                statement.setSql(sql);
                //存放占位符的参数名
                List<String> parameters = new ArrayList<>();
                //获取#{}中的值
                Pattern pattern = Pattern.compile("#\\{(.+?)\\}");
                Matcher matcher = pattern.matcher(sql);
                while (matcher.find()) {
                    parameters.add(matcher.group(1));
                }
                statement.setParameters(parameters);
//                System.out.println(statement);
                configuration.registMapperStatement(sqlId, statement);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
