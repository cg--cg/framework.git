package mybatis.session;

import mybatis.config.Configuration;

/**
 * @author cg
 * @date 2023/6/20 13:43
 * 创建sqlSession
 */
public class SqlSessionFactory {
    private Configuration configuration;

    private SqlSession sqlSession;

    public SqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
        sqlSession = new SqlSession(configuration);
    }

    public SqlSession getSqlSession() {
        return sqlSession;
    }
}
