package mybatis.annotation;

import java.lang.annotation.*;

/**
 * @author cg
 * @date 2023/6/20 14:19
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Param {
    String value();
}
