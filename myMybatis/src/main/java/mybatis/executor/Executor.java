package mybatis.executor;

import mybatis.mapper.MapperStatement;

import java.util.List;
import java.util.Map;

/**
 * @author cg
 * @date 2023/6/20 13:23
 */
public interface Executor {
    /**
     * 执行查询操作
     *
     * @param mapperStatement mapper信息
     * @param parameters   方法参数列表
     * @param <E>
     * @return 结果集
     */
    <E> List<E> query(MapperStatement mapperStatement, Map<String,Object> parameters);

    /**
     * 执行更新操作
     * @param mapperStatement
     * @param parameters
     * @return
     */
    Integer update(MapperStatement mapperStatement, Map<String, Object> parameters);
}
