package mybatis.mapper;

/**
 * @author cg
 * @date 2023/6/20 13:28
 */

import java.util.List;

/**
 * 解析mapper文件并封装参数
 */
public class MapperStatement {
    //xml对应的mapper类
    private String namespace;

    //xml对应的mapper类方法名
    private String sqlId;

    //sql语句
    private String sql;

    //返回类型
    private String resultType;

    //sql操作类型
    private SqlType sqlCommandType;

    //占位符的位置及其需要的参数名
    private List<String> parameters;

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public SqlType getSqlCommandType() {
        return sqlCommandType;
    }

    public void setSqlCommandType(SqlType sqlCommandType) {
        this.sqlCommandType = sqlCommandType;
    }
}
