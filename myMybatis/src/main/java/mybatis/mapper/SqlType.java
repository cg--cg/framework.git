package mybatis.mapper;

/**
 * @author cg
 * @date 2023/6/20 13:32
 */
public enum SqlType {
    //sql的操作类型
    SELECT("select"),
    INSERT("insert"),
    UPDATE("update"),
    DEFAULT("default");
    private String type;

    SqlType(String type) {
        this.type = type;
    }

    public String value(){
        return type;
    }

}
