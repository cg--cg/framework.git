/**
 * 
 */
package test.dao;


import mybatis.annotation.Param;
import test.bean.User;

import java.util.List;


/**
 * @author cg
 * @date 2023/6/21 10:30
 */
public interface UserMapper
{

    /**
     * 获取单个user
     */
    User getUser(@Param("userId") Long id);
    
    /**
     * 获取所有用户
     */
    List<User> getAll();

    /**
     * 更新用户
     */
    Integer updateUser(@Param("tableName") String tableName, @Param("userName") String userName, @Param("userId") String id);

    Integer insertUser(@Param("user") User user);

    Integer deleteUser(@Param("userName") String userName);
}
