package test;

import mybatis.config.Configuration;
import mybatis.session.SqlSession;
import mybatis.session.SqlSessionFactory;
import test.bean.User;
import test.dao.UserMapper;

/**
 * @author cg
 * @date 2023/6/20 13:18
 */
public class Client {
    public static void main(String[] args) {
        //获取配置类信息
        Configuration configuration = new Configuration("db.properties");
        //创建SqlSession工厂
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactory(configuration);
        SqlSession sqlSession = sqlSessionFactory.getSqlSession();

        //需要动态的去获取mapper
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

//        System.out.println(userMapper.getUser(1L));
//        System.out.println(userMapper.getAll());
//        userMapper.updateUser("user", "王五", "1");
//        userMapper.deleteUser("李四");
        userMapper.insertUser(new User(null, "李四", "111", "img/1.jpg"));

    }
}
